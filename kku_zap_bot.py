import logging
import re

import bs4
import psycopg2
import requests
from bs4 import BeautifulSoup
from bs4 import NavigableString

from telegram import Update, ForceReply, ParseMode, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext, CallbackQueryHandler

API_KEY = "2145638150:AAFn9Yz4srkgVa9Vv5KIt7f608EyWSoU054"

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments update and
# context.
def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    update.message.reply_markdown_v2(
        fr'Hi {user.mention_markdown_v2()}\!',
        reply_markup=ForceReply(selective=True),
    )


def help_command(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""

    update.message.reply_text('Help!')


def callback_type(update: Update, context: CallbackContext) -> None:
    """Parses the CallbackQuery and updates the message text."""
    # query = update.callback_query
    answer_data = update.callback_query.data
    res = answer_data.split(':')[0]
    if (res == "part_info"): call_chose_part(update, context)
    if (res == "add_part"): call_add_part(update, context)


def call_chose_part(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    answer_data = update.callback_query.data
    part_info = answer_data.split(':')[1]
    keyboard = [
        [
            InlineKeyboardButton("Добавить", callback_data="add_part:" + part_info),
            InlineKeyboardButton("Нет", callback_data='2')
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text('Добавить в перечень запчастей: \n https://exist.ru' + part_info, reply_markup=reply_markup)


def call_add_part(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    answer_data = update.callback_query.data
    part_link = answer_data.split(':')[1]
    url = "https://www.exist.ru" + part_link
    html_text = requests.get(url).text
    soup = BeautifulSoup(html_text, 'html.parser')
    cdata = soup.find(text=re.compile("CDATA"))
    dict_string = re.search('{(.*)}', cdata).group(1)
    var_data = eval("{" + dict_string.replace("false", "False").replace("true", "True") + "}")
    insert_part(var_data['PartNumber'], var_data['Description'],
                var_data['ProductIdEnc'], var_data['ProdUrl'],
                var_data['maxPrice'], var_data['minPrice'])

    query.edit_message_text("Добавлено")


def insert_part(input_part_number, input_description, input_pid_exist, input_prod_url_exist, input_max_price,
                input_min_price):
    # sql_insert_part = f"INSERT INTO GARAGE_PARTS.PARTS(GID, PART_NUMBER, PPN_DT) SELECT nextval('GARAGE_PARTS.parts_gids') as gid, {input_part_number} as part_number, current_timestamp as ppn_dt"
    sql_insert_part = f"""
            INSERT INTO garage_parts.pparts
            (gid, part_number, description, pid_exist, prod_url_exist, max_price, min_price, ppn_dt_tm)
            SELECT nextval('GARAGE_PARTS.parts_gids') as gid, {input_part_number} as part_number, {input_description} as description, {input_pid_exist} as pid_exist,
             {input_prod_url_exist} as prod_url_exist, {input_max_price} as max_price, {input_min_price} as min_price, current_timestamp as ppn_dt
            """
    conn = psycopg2.connect(dbname='kku_zap_db', user='postgres', password='123456', host='localhost')
    cursor = conn.cursor()
    cursor.execute(sql_insert_part)
    conn.commit()


def echo(update: Update, context: CallbackContext) -> None:
    """Echo the user message."""
    response_ = requests.get("https://www.exist.ru/Price/?pcode=" + update.message.text)
    # update.message.reply_text("https://www.exist.ru/Price/?pcode=" + update.message.text)

    vgm_url = "https://www.exist.ru/Price/?pcode=" + update.message.text
    html_text = requests.get(vgm_url).text
    soup = BeautifulSoup(html_text, 'html.parser')
    a = soup.find_all("div", {"class": "page-blocks page-blocks--padding page-content-wrapper --catalogs"})
    html_search_result = a[0].contents[3]

    message_text = ""
    keyboard = [
    ]

    # keyboard.append("qwe")

    for part in html_search_result.contents:
        if part != "\n":
            part_attr = part.contents[1]  # html найденной запчасти
            part_link_to_exist = part_attr.get("href")
            so = BeautifulSoup(str(part_attr), 'html.parser')
            company_name = so.find('b').text
            part_name = so.find('dd').text
            keyboard.append([InlineKeyboardButton(company_name + ": " + part_name,
                                                  callback_data="part_info:" + part_link_to_exist)])
            # message_text = message_text + part_link_to_exist + '\n' + company_name + '\n' + part_name + '\n\n'

    print(keyboard)
    reply_markup = InlineKeyboardMarkup(keyboard)

    update.message.reply_text('Please choose:', reply_markup=reply_markup)

    # update.message.reply_text(message_text)
    # update.message.reply_text("https://www.exist.ru/Price/?pcode=" + update.message.text)


def main() -> None:
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(API_KEY)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CallbackQueryHandler(callback_type))

    # on non command i.e message - echo the message on Telegram
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
